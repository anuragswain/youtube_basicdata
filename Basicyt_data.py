
from apiclient.discovery import build
#from apiclient.errors import HttpError
#from oauth2client.tools import argparser
import pandas as pd
#import pprint 
#import matplotlib.pyplot as pd
import datetime as DT


DEVELOPER_KEY = "AIzaSyDR9QmTaZO0a6SNqiDHt0jKwPD_FPVRvrw"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

def youtube_search(q, max_results=50,order="relevance", token=None, location=None, location_radius=None):

    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,developerKey=DEVELOPER_KEY)

    search_response = youtube.search().list(
    q=q,
    type="video",
    pageToken=token,
    order = order,
    part="id,snippet", # Part signifies the different types of data you want 
    maxResults=max_results,
    location=location,
    locationRadius=location_radius).execute()

    title = []
    channelId = []
    channelTitle = []
    categoryId = []
    videoId = []
    viewCount = []
    likeCount = []
    dislikeCount = []
    commentCount = []
    favoriteCount = []
    publishedAt = [] ##my stuff
    description= [] ##my stuff
   # category = []
    tags = []
    #videos = []
    
    for search_result in search_response.get("items", []):
    	if search_result["id"]["kind"] == "youtube#video":

            title.append(search_result['snippet']['title']) 

            videoId.append(search_result['id']['videoId'])

            response = youtube.videos().list(
                part='statistics, snippet',
                id=search_result['id']['videoId']).execute()

            channelId.append(response['items'][0]['snippet']['channelId'])
            channelTitle.append(response['items'][0]['snippet']['channelTitle'])
            categoryId.append(response['items'][0]['snippet']['categoryId'])
            favoriteCount.append(response['items'][0]['statistics']['favoriteCount'])
            viewCount.append(response['items'][0]['statistics']['viewCount'])
            likeCount.append(response['items'][0]['statistics']['likeCount'])
            dislikeCount.append(response['items'][0]['statistics']['dislikeCount'])
            publishedAt.append(response['items'][0]['snippet']['publishedAt']) ##my stuff
            description.append(response['items'][0]['snippet']['description'].encode('utf8')) ##my stuff
    
        if 'commentCount' in response['items'][0]['statistics'].keys():
            commentCount.append(response['items'][0]['statistics']['commentCount'])
        else:
            commentCount.append([])
	  
        if 'tags' in response['items'][0]['snippet'].keys():
            tags.append(response['items'][0]['snippet']['tags'])
        else:
            tags.append([])

    youtube_dict = {'tags':tags,'channelId': channelId,'channelTitle': channelTitle,'categoryId':categoryId,'title':title,'videoId':videoId,'viewCount':viewCount,'likeCount':likeCount,'dislikeCount':dislikeCount,'commentCount':commentCount,'favoriteCount':favoriteCount,'publishedAt':publishedAt,'description':description} ## publishedAt and description i have added 
    
    return youtube_dict

Video_n_CreditDetails=youtube_search("Being Indian")
Video_n_CreditDetails_df=pd.DataFrame(Video_n_CreditDetails)

CreditDetails=Video_n_CreditDetails_df['description']

#Cast_index=[]
#Cast=[]

#### for finding authors
#for description in CreditDetails:
#    Cast_index=description.find("Cast")
#    Cinematographer=description.find("Cinematographer")
#    #print(Cast_index)
#    
#    Length=[]
#    
#    for line in (description.split('\n')):
#        Length.append(len(line))
#        
#    Cast.append(description[Cast_index:Cinematographer])



DatenTime=[]
temp=[]

for datepublish in Video_n_CreditDetails_df['publishedAt']:
    temp=datepublish.split("T")
    DatenTime.append(temp[0])

    
#Video_n_CreditDetails_df['Cast']=Cast
Video_n_CreditDetails_df['Date']=DatenTime

Video_n_CreditDetails_df.to_csv("BI_descnew.csv")   
today = DT.date.today()
week_ago = today - DT.timedelta(days=7)

CurrentweekVideos=Video_n_CreditDetails_df[(Video_n_CreditDetails_df['Date']>=str(week_ago))&(Video_n_CreditDetails_df['Date']>=str(week_ago))]                    
CurrentweekVideos.to_csv("CurrentVideos.csv")