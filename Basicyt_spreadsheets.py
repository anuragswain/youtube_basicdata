import gspread
from oauth2client.service_account import ServiceAccountCredentials


# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com']
creds = ServiceAccountCredentials.from_json_keyfile_name('Youtube-basicdata-28cbc2191e82.json', scope)
client = gspread.authorize(creds)

# Find a workbook by name and open the first sheet
# Make sure you use the right name here.
sheet = client.open("Youtube_basicdata_spreadsheets_wekklyappend").Youtube_basicdata

# Extract and print all of the values
#records = sheet.get_all_records()
#print(records)


